extends Spatial

var speed = 5
var target_position : Vector3

func _physics_process(delta):
	if(target_position != Vector3(0, 0, 0)):
		var direction = transform.origin.direction_to(target_position)
		transform.origin.x += direction.x * delta * speed
		transform.origin.z += direction.z * delta * speed
		if(target_position.distance_to(transform.origin) < 0.1):
			target_position = Vector3()

func animate_to(position):
	match(position):
		'left':
			target_position = Vector3(-1, 2, 0)
		'right':
			target_position = Vector3(1, 2, 0)
		'down':
			target_position = Vector3(0, 2, 0.75)
		'up':
			target_position = Vector3(0, 2, -0.75)

func _unhandled_input(event):
	var keys = SpoonKeyEnum.action.keys()
	if event.is_action_pressed(keys[SpoonKeyEnum.action.right]):
		animate_to('right')
	if event.is_action_pressed(keys[SpoonKeyEnum.action.left]):
		animate_to('left')
	if event.is_action_pressed(keys[SpoonKeyEnum.action.up]):
		animate_to('up')
	if event.is_action_pressed(keys[SpoonKeyEnum.action.down]):
		animate_to('down')

func _on_SponMechanic_key_pressed(success : bool, key_name : String):
	if success:
		animate_to(key_name)
