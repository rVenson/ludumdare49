extends Node

signal key_pressed
signal key_changed

const DEFAULT_DELAY_ACTION = 1.0
var next_key_action = 0
var next_time_action = DEFAULT_DELAY_ACTION

func _ready():
	randomize()

func swap_key():
	var index = randi() % SpoonKeyEnum.action.size()
	if(next_key_action == index): # check if the key is the same
		if(index == 0): # check if 0 and sum one or subtract one
			index += 1
		else:
			index -= 1
	var keys = SpoonKeyEnum.action.values()
	next_key_action = keys[index]
	next_time_action = DEFAULT_DELAY_ACTION
	emit_signal('key_changed', next_key_action)

func _physics_process(delta):
	next_time_action = next_time_action - delta
	if next_time_action < 0:
		emit_signal('key_pressed', false)
		swap_key()

func _unhandled_key_input(event : InputEventKey):
	var keys = SponKeyEnum.action.keys()
	if (event.is_action_pressed(keys[SponKeyEnum.action.right])
			|| event.is_action_pressed(keys[SponKeyEnum.action.left])
			|| event.is_action_pressed(keys[SponKeyEnum.action.down])
			|| event.is_action_pressed(keys[SponKeyEnum.action.up])):
		var key_name = keys[next_key_action]
		if event.is_action_pressed(key_name):
			emit_signal('key_pressed', true, key_name)
		else:
			emit_signal('key_pressed', false, key_name)
		swap_key()
