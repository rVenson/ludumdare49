class_name KeyPanel
extends Panel

signal key_success

onready var _label = $Label

const speed = 100.0
var key : int
var key_name : String
var HIT_LIMIT = 0.5
var tickets : float

enum state {
	RUNNING,
	IDEAL,
	LATE
}

var current_state = state.RUNNING

var default_color = Color('71ffffff')
var selected_color = Color.rebeccapurple
var success_color = Color.greenyellow
var error_color = Color.red

func set_name(text):
	_label.text = text

func color_key(color : Color):
	set_self_modulate(color)

func reset_color():
	set_self_modulate(default_color)
