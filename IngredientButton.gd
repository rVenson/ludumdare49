class_name IngredientButton
extends Button

onready var labeled_texture : LabeledTexture = $LabeledTexture

func _ready():
	labeled_texture.update_ui_values()
