extends Node

var current_scene
var selected_potion = 'smell'

var scenes = {
	"menu": preload("res://MainMenu.tscn"),
	"briefing": preload("res://Briefing.tscn"),
	"level": preload("res://Game.tscn")
}

func _ready():
	get_tree().paused = false

func start_game(potion : String):
	selected_potion = potion
	get_tree().change_scene_to(scenes.level)

func start_briefing():
	get_tree().change_scene_to(scenes.briefing)

func main_menu():
	get_tree().change_scene_to(scenes.menu)
