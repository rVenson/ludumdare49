extends Control

onready var _container = $VBoxContainer

onready var _label = {
	"spider": $VBoxContainer/spider/HBoxContainer/Label,
	"bat": $VBoxContainer/bat/HBoxContainer/Label,
	"frog": $VBoxContainer/frog/HBoxContainer/Label,
	"mushroom": $VBoxContainer/mushroom/HBoxContainer/Label,
	"deer": $VBoxContainer/deer/HBoxContainer/Label,
	"coins": $VBoxContainer/coins/HBoxContainer/Label
}

func _ready():
	update_ui_values()
	PlayerData.connect("storage_changed", self, "_storage_changed")
	register_all_buttons()

func register_all_buttons():
	for child in _container.get_children():
		child.connect('pressed', self, '_on_button_pressed', [child.name])

func update_ui_values():
	for l in _label:
		update_ui_single_value(l, str(PlayerData.storage[l]))

func update_ui_single_value(label : String, value : String):
	_label[label].text = value

func _storage_changed(type : String, new_quantity : int, old_quantity : int):
	update_ui_single_value(type, str(new_quantity))

func _on_button_pressed(slug : String):
	if slug == 'coins':
		return
	if PlayerData.storage[slug] > 0:
		AudioPlayer.play_audio('unclick')
		var price = Data.ingredients[slug].price
		PlayerData.add_storage_value(slug, -1)
		PlayerData.add_storage_value('coins', price)
