extends Control

func _on_TextureButton_pressed():
	get_tree().paused = false
	AudioPlayer.play_audio('failure')
	GameManager.start_briefing()
