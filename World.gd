class_name LevelWorld
extends Spatial

onready var witch = $Witch
onready var spoon = $Spoon
onready var witch_hand_l : Spatial = $Witch/Witch/RootNode/handL
onready var witch_hand_r : Spatial = $Witch/Witch/RootNode/handR
onready var spoon_position : Spatial = $Spoon/HandsPosition
onready var particles : Particles = $Cauldron/Particles
onready var cauldron_liquid : Spatial = $Cauldron/CauldronLiquid
onready var cauldron_audio : AudioStreamPlayer = $Cauldron/AudioStreamPlayer

onready var witch_hand_l_original_pos = witch_hand_l.global_transform.origin
onready var witch_hand_r_original_pos = witch_hand_r.global_transform.origin

func _ready():
	set_physics_process(false)

func attach_spoon():
	set_physics_process(true)

func _physics_process(delta):
	update_hand_position()

func show_visual_effects(value : bool):
	cauldron_audio.play()
	cauldron_liquid.visible = value
	particles.emitting = value

func increase_visual_effects():
	cauldron_liquid.transform.origin.y += 0.12
	particles.amount += 4

func update_hand_position():
	witch_hand_l.global_transform.origin = spoon_position.global_transform.origin
	witch_hand_r.global_transform.origin = spoon_position.global_transform.origin
	witch_hand_r.global_transform.origin.y += 1
