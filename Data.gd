extends Node

var ingredients_texture = {
	"bat": preload("res://assets/textures/bat_icon.png"),
	"spider": preload("res://assets/textures/spider_icon.png"),
	"frog": preload("res://assets/textures/frog_icon.png"),
	"mushroom": preload("res://assets/textures/mushroom_icon.png"),
	"deer": preload("res://assets/textures/deer_icon.png")
}

var potions = {
	"smell": {
		"slug": "smell",
		"name": "Bad Smell Potion",
		"sell_price": 100,
		"color": Color("#ffffff"),
		"ingredients": [
			{
				"slug": "spider",
				"quantity": 3
			},
			{
				"slug": "bat",
				"quantity": 3
			}
		]
	},
	"tummyache": {
		"slug": "tummyache",
		"name": "Tummyache Potion",
		"sell_price": 180,
		"color": Color("#56eef2"),
		"ingredients": [
			{
				"slug": "spider",
				"quantity": 2
			},
			{
				"slug": "bat",
				"quantity": 2
			},
			{
				"slug": "frog",
				"quantity": 2
			}
		]
	},
	"insominia": {
		"slug": "insominia",
		"name": "Insomnia Potion",
		"sell_price": 250,
		"color": Color("#fdff1e"),
		"ingredients": [
			{
				"slug": "bat",
				"quantity": 1
			},
			{
				"slug": "frog",
				"quantity": 1
			},
			{
				"slug": "mushroom",
				"quantity": 3
			}
		]
	},
	"headache": {
		"slug": "headache",
		"name": "Headache Potion",
		"sell_price": 400,
		"color": Color("#293ec4"),
		"ingredients": [
			{
				"slug": "mushroom",
				"quantity": 5
			}
		]
	},
	"love": {
		"slug": "love",
		"name": "Love Potion",
		"sell_price": 500,
		"color": Color("#e255ff"),
		"ingredients": [
			{
				"slug": "bat",
				"quantity": 2
			},
			{
				"slug": "frog",
				"quantity": 1
			},
			{
				"slug": "mushroom",
				"quantity": 1
			},
			{
				"slug": "deer",
				"quantity": 2
			}
		]
	}
}

var ingredients = {
	"spider": {
		"slug": "spider",
		"name": "Shy Spider Legs",
		"spoon": 3,
		"acid": 1,
		"price": 10
	},
	"bat": {
		"slug": "bat",
		"name": "Groggy Bat Wings",
		"spoon": 4,
		"acid": 3,
		"price": 15
	},
	"frog": {
		"slug": "frog",
		"name": "Lazy Frog Legs",
		"spoon": 7,
		"acid": 5,
		"price": 25
	},
	"mushroom": {
		"slug": "mushroom",
		"name": "Rotten Moon Mushroom",
		"spoon": 7,
		"acid": 5,
		"price": 50
	},
	"deer": {
		"slug": "deer",
		"name": "Insane Deer Antlers",
		"spoon": 10,
		"acid": 10,
		"price": 100
	}
}
