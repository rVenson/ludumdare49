extends Control

onready var label = $Label
onready var button = $TextureButton

var lines = {
	"start": [],
	"final": [],
}

func start_story(story_name : String):
	show()
	for line in lines[story_name]:
		label.text = str(line)
		yield(button, "pressed")
	hide()
