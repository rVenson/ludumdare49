extends Control

var state = 0
onready var right_spoons = $VBoxContainer/RightSpoons
onready var wrong_spoons = $VBoxContainer/WrongSpoons
onready var controled_fire = $VBoxContainer/ControledFire
onready var max_acidity = $VBoxContainer/MaxAcidity
onready var time = $VBoxContainer/Time
onready var continue_label = $Continue

onready var label = {
	"right_spoons": $VBoxContainer/RightSpoons/Value,
	"wrong_spoons": $VBoxContainer/WrongSpoons/Value,
	"fire_in_control": $VBoxContainer/ControledFire/Value,
	"max_acid": $VBoxContainer/MaxAcidity/Value,
	"time": $VBoxContainer/Time/Value
}

func show_win(potion_name, game_status):
	PlayerData.story_mode[potion_name] = true
	for l in label:
		label[l].text = str(game_status[l])
	show()

func _on_TextureButton_pressed():
	match(state):
		0:
			right_spoons.show()
		1:
			wrong_spoons.show()
		2:
			controled_fire.show()
		3:
			max_acidity.show()
		4:
			time.show()
		5:
			continue_label.show()
		6:
			get_tree().paused = false
			GameManager.start_briefing()
	state += 1
