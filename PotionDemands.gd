class_name PotionDemands
extends Control

export var _potion_button_resource : Resource
onready var _grid = $CenterContainer/GridContainer
onready var group = ButtonGroup.new()

func _ready():
	load_all_ingredients()
	update_ui()
	PlayerData.connect("storage_changed", self, '_storage_changed')

func get_selected_potion():
	var pressed_button : PotionItemButton = group.get_pressed_button()
	if pressed_button:
		return pressed_button.slug
	return null

func load_all_ingredients():
	for key in Data.potions:
		var potion = Data.potions[key]
		var button : PotionItemButton = _potion_button_resource.instance()
		button.group = group
		button.title = potion.name
		button.slug = key
		button.price = potion.sell_price
		button.potion_color = potion.color
		_grid.add_child(button)
		for ingredient in potion.ingredients:
			button.add_ingredient(Data.ingredients_texture[ingredient.slug], ingredient.quantity, ingredient.slug)

func update_ui():
	for button in _grid.get_children():
		button.disabled = false
		for ingredient in Data.potions[button.slug].ingredients:
			if(ingredient.quantity > PlayerData.storage[ingredient.slug]):
				button.disabled = true

func _storage_changed(type, new_quantity, old_quantity):
	update_ui()
