extends Node

signal acid_level_changed

onready var _start_ui = $StartGameUI
onready var _win_ui = $WinUI
onready var _game_over_ui = $GameOverUI
onready var _mechanic_ui = $MechanicUI
onready var _fire_ui = $MechanicUI/FireUI
onready var _spoon_ui = $MechanicUI/SpoonUI
onready var _inventory_ui = $MechanicUI/InventoryUI
onready var _world = $LevelWorld

var acid_level = 0
var number_of_started_ingredients
var left_ingredients : int
var potion_name
var is_started = false
var game_status = {
	"right_spoons": 0,
	"wrong_spoons": 0,
	"max_acid": 0,
	"fire_in_control": true,
	"time": 0
}

func _ready():
	set_physics_process(false)
	potion_name = GameManager.selected_potion
	var ingredients = Data.potions[potion_name].ingredients
	number_of_started_ingredients = count_left_ingredients(ingredients)
	_inventory_ui.load_ingredients(ingredients)
	if !PlayerData.story_mode[potion_name]:
		_start_ui.start_story(potion_name)

func start_potion():
	_world.show_visual_effects(true)
	_world.attach_spoon()
	var fire_random_value = ((randi() % 8) * 10) + 15
	_fire_ui.change_optimal_fire(fire_random_value)
	_fire_ui.add_fire(fire_random_value)
	set_physics_process(true)

func _physics_process(delta):
	apply_fire_state(delta)
	game_status.time += delta

func apply_fire_state(delta):
	match(_fire_ui.fire_state):
		FireStateEnum.state.optimal:
			Engine.time_scale = 1
			add_acid(delta/5)
		FireStateEnum.state.low:
			game_status.fire_in_control = false
			add_acid(delta)
		FireStateEnum.state.high:
			Engine.time_scale = 2
			game_status.fire_in_control = false
			add_acid(delta)

func count_left_ingredients(ingredients : Array):
	for i in ingredients:
		left_ingredients += i.quantity
	return left_ingredients

func add_acid(amount : float):
	acid_level += amount
	emit_signal("acid_level_changed", acid_level)
	
	if game_status.max_acid < acid_level:
		game_status.max_acid = acid_level
	
	if(acid_level > 20):
		game_over()

func _on_SpoonUI_key_hit(success : bool):
	if success:
		game_status.right_spoons += 1
		add_acid(-1)
	else:
		game_status.wrong_spoons += 1
		add_acid(1)

func _on_InventoryUI_ingredient_selected(slug : String):
	if !is_started:
		is_started = true
		start_potion()
	_spoon_ui.generate_chords(Data.ingredients[slug].spoon)
	add_acid(Data.ingredients[slug].acid)
	left_ingredients -= 1
	_world.increase_visual_effects()
	

func _on_SpoonUI_no_running_keys():
	if(left_ingredients <= 0):
		if acid_level <= 20:
			win()

func game_over():
	set_physics_process(false)
	AudioPlayer.play_audio('explosion')
	_game_over_ui.visible = true
	_mechanic_ui.visible = false
	get_tree().paused = true

func win():
	set_physics_process(false)
	AudioPlayer.play_audio('success')
	_win_ui.show_win(potion_name, game_status)
	_mechanic_ui.visible = false
	get_tree().paused = true
	
	PlayerData.storage.coins += Data.potions[potion_name].sell_price
	var ingredients = Data.potions[potion_name].ingredients
	for i in ingredients:
		PlayerData.storage[i.slug] -= i.quantity
