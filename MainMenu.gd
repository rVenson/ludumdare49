extends Control

onready var credits = $Credits
onready var continue_button = $Menu/VBoxContainer/ContinueButton

func _ready():
	if PlayerData.is_game_initialized:
		continue_button.visible = true
	else:
		continue_button.visible = false

func _on_StartButton_pressed():
	PlayerData.reset_data()
	GameManager.start_briefing()

func _on_ExitButton_pressed():
	get_tree().quit(-1)

func _on_ContinueButton_pressed():
	GameManager.start_briefing()

func _on_Back_pressed():
	credits.hide()

func _on_CreditsButton_pressed():
	credits.show()
