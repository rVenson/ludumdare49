class_name LabeledTexture
extends HBoxContainer

onready var _texture = $TextureRect
onready var _label = $Label

var description : String
var texture : Texture
var value = 0

func _ready():
	if(texture):
		update_ui_values()
	
func update_ui_values():
	hint_tooltip = str(description)
	_texture.texture = texture
	_label.text = str(value)
