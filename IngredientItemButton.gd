extends Button
class_name IngredientItemButton

onready var _texture = $VBoxContainer/TextureRect
onready var _title_label = $VBoxContainer/Title
onready var _acid_label = $VBoxContainer/HBoxContainer/Control/Label
onready var _spoon_label = $VBoxContainer/HBoxContainer/Control2/Label
onready var _price_label = $VBoxContainer/HBoxContainer/Control3/Label

export var title : String = "Default Ingredient"
export var slug : String  = "default"
export var ingredient_texture : Texture
export var acid_value = 0
export var spoon_value = 0
export var price_value = 0

func _ready():
	update_ui_values()

func update_ui_values():
	if(ingredient_texture):
		_texture.texture = ingredient_texture
	_title_label.text = str(title)
	_acid_label.text = str(acid_value)
	_spoon_label.text = str(spoon_value)
	_price_label.text = str(price_value)

func _pressed():
	AudioPlayer.play_audio('click')
