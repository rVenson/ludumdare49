extends Control

onready var _slider : CustomSlider = $AcidSlider

func _on_GameLoop_acid_level_changed(value : float):
	_slider.set_value(value)

func _on_InventoryUI_ingredient_hover(slug : String, status : bool):
	var ingredient = Data.ingredients[slug]
	if status:
		var value = ingredient.acid / 2.0
		_slider.set_optimal_size(value)
		_slider.set_optimal_value(20 - value)
		_slider.show_optimal('Acid +' + str(ingredient.acid), true)
	else:
		_slider.show_optimal('', false)
