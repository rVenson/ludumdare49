extends Control

var current_key
onready var result_label = $Label
onready var keysUI = [
	$KeyLeft,
	$KeyRight,
	$KeyUp,
	$KeyDown
]

func _on_SponMechanic_key_changed(key_number : int):
	if(current_key != null):
		keysUI[current_key].reset_color()
	keysUI[key_number].color_key()
	current_key = key_number

func _on_SponMechanic_key_pressed(sucess : bool, key_name : String):
	if(sucess):
		result_label.text = "Acertou!"
	else:
		result_label.text = "Errou!"
