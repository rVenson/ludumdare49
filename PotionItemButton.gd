extends Button
class_name PotionItemButton

onready var _texture = $VBoxContainer/TextureRect
onready var _title_label = $VBoxContainer/Title
onready var _price_label = $VBoxContainer/Control3/Label2
onready var _container = $VBoxContainer/HBoxContainer

export var labeled_texture_resource : Resource
export var title : String = "Default Potion"
export var slug : String
export var potion_color : Color
export var price = 100

func _ready():
	update_ui_values()

func update_ui_values():
	_texture.self_modulate = potion_color
	_title_label.text = str(title)
	_price_label.text = str(price)

func add_ingredient(texture : Texture, value : int, description : String = ""):
	var labeled : LabeledTexture = labeled_texture_resource.instance()
	labeled.description = description
	labeled.texture = texture
	labeled.value = value
	_container.add_child(labeled)

func _pressed():
	AudioPlayer.play_audio('click')
