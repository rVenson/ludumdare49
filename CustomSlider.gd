class_name CustomSlider
extends Control

export var max_value = 100
export var optimal_size = 10

onready var _optimal_slider = $OptimalSlider
onready var _optimal_label = $OptimalSlider/Label
onready var _slider = $Slider
onready var _slider_label = $TextureRect/Label

onready var total_size = 300
onready var ticket_size = total_size / max_value
var value = 0

func set_optimal_size(size : float):
	optimal_size = size

func set_optimal_value(value):
	var optimal_top = value - optimal_size
	var optimal_bottom = (max_value - optimal_size) - value
	print(optimal_top, ', ', optimal_bottom)
	_optimal_slider.margin_bottom = -(optimal_top) * ticket_size
	_optimal_slider.margin_top = -total_size + (optimal_bottom * ticket_size)

func set_optimal_text(text):
	_optimal_label.text = text

func set_value(value):
	value = value
	_slider.rect_size.y = value * ticket_size
	_slider.rect_position.y = total_size - (value * ticket_size)
	_slider_label.text = str(int(value))

func show_optimal(text : String, status : bool = true):
	_optimal_slider.visible = status
	set_optimal_text(text)
