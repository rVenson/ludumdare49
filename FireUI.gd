extends Control

onready var slider : CustomSlider = $FireSlider
onready var label : Label = $Label

const DEFAULT_FIRE_DECAY = 2
var fire_satisfaction
var fire_range = 10
var optimal_fire_level = 50
var current_fire_level = 0
var fire_state = FireStateEnum.state.optimal

func _physics_process(delta):
	if(current_fire_level > 0):
		add_fire(-DEFAULT_FIRE_DECAY * delta)
		calculate_fire_damage()

func calculate_fire_damage():
	if (optimal_fire_level + fire_range) < current_fire_level: # high fire
			fire_state = FireStateEnum.state.high
	else:
		if (optimal_fire_level - fire_range) > current_fire_level: # low fire
			fire_state = FireStateEnum.state.low
		else: # optimal fire
			fire_state = FireStateEnum.state.optimal

func change_optimal_fire(amount):
	optimal_fire_level = amount
	slider.set_optimal_value(optimal_fire_level)

func add_fire(amount):
	current_fire_level += amount
	if(current_fire_level > 100):
		current_fire_level = 100
	if(current_fire_level < 0):
		current_fire_level = 0
	fire_satisfaction = abs(current_fire_level - optimal_fire_level) - fire_range
	slider.set_value(current_fire_level)
	if(fire_satisfaction < 0):
		slider.set_optimal_text("Optimal Fire")
	else:
		slider.set_optimal_text("Out!")

func _unhandled_key_input(event : InputEventKey):
	if event.is_action_pressed("ui_select"):
		AudioPlayer.play_audio('fire')
		add_fire(10)
