extends Control

signal ingredient_selected
signal ingredient_hover

export var ingredient_button_resource : Resource
onready var _container = $HBoxContainer

func load_ingredients(ingredients : Array):
	for ingredient in ingredients:
		var ingredient_button : IngredientButton = ingredient_button_resource.instance()
		_container.add_child(ingredient_button)
		ingredient_button.labeled_texture.description = ingredient.slug
		ingredient_button.labeled_texture.texture = Data.ingredients_texture[ingredient.slug]
		ingredient_button.labeled_texture.value = ingredient.quantity
		ingredient_button.labeled_texture.update_ui_values()
		ingredient_button.connect("pressed", self, '_ingredient_selected', [ingredient.slug, ingredient_button])
		ingredient_button.connect("mouse_exited", self, '_ingredient_hover', [ingredient.slug, false])
		ingredient_button.connect("mouse_entered", self, '_ingredient_hover', [ingredient.slug, true])

func _ingredient_selected(slug : String, button : IngredientButton):
	AudioPlayer.play_audio('ingredient')
	emit_signal("ingredient_selected", slug)
	button.labeled_texture.value -= 1
	if(button.labeled_texture.value <= 0):
		button.queue_free()
		emit_signal("ingredient_hover", slug, false)
	else:
		button.labeled_texture.update_ui_values()

func _ingredient_hover(slug : String, status : bool):
	emit_signal("ingredient_hover", slug, status)
