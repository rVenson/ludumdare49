extends Control

onready var final_story = $FinalStory
onready var _potion_menu : PotionDemands = $PotionDemands

func _ready():
	if PlayerData.story_mode.love and !PlayerData.story_mode.final:
		final_story.start_story('final')
	if !PlayerData.story_mode.smell:
		final_story.start_story('start')

func _on_StartButton_pressed():
	var potion_name = _potion_menu.get_selected_potion()
	if potion_name:
		GameManager.start_game(potion_name)

func _on_BackButton_pressed():
	GameManager.main_menu()
