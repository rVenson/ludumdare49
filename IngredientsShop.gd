extends Control

export var _item_button_resource : Resource
onready var _grid = $CenterContainer/GridContainer

func _ready():
	load_all_ingredients()
	PlayerData.connect("storage_changed", self, "_storage_changed")

func load_all_ingredients():
	for key in Data.ingredients:
		var ingredient = Data.ingredients[key]
		var button : IngredientItemButton = _item_button_resource.instance()
		button.ingredient_texture = Data.ingredients_texture[ingredient.slug]
		button.title = ingredient.name
		button.slug = ingredient.slug
		button.acid_value = ingredient.acid
		button.spoon_value = ingredient.spoon
		button.price_value = ingredient.price
		button.connect("pressed", self, "_item_selected", [button])
		_grid.add_child(button)
	update_ui()

func update_ui():
	for button in _grid.get_children():
		button.disabled = false
		var ingredient = Data.ingredients[button.slug]
		if ingredient.price > PlayerData.storage.coins:
				button.disabled = true

func _item_selected(button : IngredientItemButton):
	if button.price_value <= PlayerData.storage.coins:
		PlayerData.add_storage_value('coins', -button.price_value)
		PlayerData.add_storage_value(button.slug, 1)
	update_ui()

func _storage_changed(type, new_quantity, old_quantity):
	update_ui()
