extends Node

signal storage_changed
var is_game_initialized = false

var story_mode = {
	"smell": false,
	"tummyache": false,
	"insominia": false,
	"headache": false,
	"love": false,
	"final": false
}

var storage = {
		"spider": 0,
		"bat": 0,
		"frog": 0,
		"mushroom": 0,
		"deer": 0,
		"coins": 75
	}

func reset_data():
	is_game_initialized = true
	storage = {
		"spider": 0,
		"bat": 0,
		"frog": 0,
		"mushroom": 0,
		"deer": 0,
		"coins": 75
	}

func change_storage_value(type : String, new_quantity : int):
	var old_quantity = storage[type]
	storage[type] = new_quantity
	emit_signal("storage_changed", type, storage[type], old_quantity)

func add_storage_value(type : String, increment : int):
	var old_quantity = storage[type]
	storage[type] += increment
	emit_signal("storage_changed", type, storage[type], old_quantity)
