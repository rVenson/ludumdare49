extends Node

var audio = {
	"explosion": preload("res://assets/sound/fx/explosion.wav"),
	"left": preload("res://assets/sound/fx/note01.wav"),
	"right": preload("res://assets/sound/fx/note02.wav"),
	"up": preload("res://assets/sound/fx/note03.wav"),
	"down": preload("res://assets/sound/fx/note04.wav"),
	"failure": preload("res://assets/sound/fx/failure.wav"),
	"success": preload("res://assets/sound/fx/success.wav"),
	"ingredient": preload("res://assets/sound/fx/ingredient.wav"),
	"mute_note": preload("res://assets/sound/fx/mute_note.wav"),
	"fire": preload("res://assets/sound/fx/fire.wav"),
	"click": preload("res://assets/sound/fx/click.wav"),
	"unclick": preload("res://assets/sound/fx/unclick.wav")
}

func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS

func play_audio(name):
	if audio.has(name):
		var player : AudioStreamPlayer = AudioStreamPlayer.new()
		add_child(player)
		player.stream = audio[name]
		player.play()
