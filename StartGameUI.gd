extends Control

onready var label = $Label
onready var button = $TextureButton

var lines = {
	"smell": [
		"A young woman went to the witch's house clamoring for a solution to her embarrassment.",
		"She wanted to get the attention of a cute city boy. \n\n- What can you do to make him notice me, lady witch?",
		"\"I have what you need\" - claim the witch. - give me a few minutes and I'm sure that no guy will pass by you without noticing"
	],
	"tummyache": [
		"Two children came to the witch's house and demanded that she make chocolate cakes and candies.",
		"We want candy! - screamed the furious children",
		"What ill-bred children - exclaimed the witch."
	],
	"insominia": [
		"Twice a year, some weird men and women seek out the witch for help",
		"\"We need help, lady witch! We won't be able to finish our game in time.\"",
		"\"A game? In 48 hours? How crazy!\" - the witch never got used to the strange habits of this village.",
	],
	"headache": [
		"Yes, just another potion order...",
		"What do you expect? I have a headache, I won't go on with this story.",
	],
	"love": [
		"One day, a mysterious traveler caught the witch's attention in the city's traditional fair.",
		"Realizing that he had been noticed, the traveler approached the witch but said nothing.",
		"The witch was about to curse when the mysterious traveler left her a paper with a request",
		"It was a love potion"
	],
}

func start_story(story_name : String):
	show()
	for line in lines[story_name]:
		label.text = str(line)
		yield(button, "pressed")
	hide()
