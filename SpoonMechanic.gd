extends Node

signal key_hit
signal no_running_keys

export var key_panel_resource : Resource
onready var _keys_group = $KeysGroup

var keys_queued = []
var running_keys = []
var secs_until_next_key = 0

const BASE_TIME = 4.0
const BASE_LATE_TIME = 1.0
const BASE_DELAY_MIN = 0.75
const BASE_DELAY_MAX = 2.0

func _ready():
	randomize()

func _unhandled_key_input(event : InputEventKey):
	if running_keys.size() > 0:
		var keys = SpoonKeyEnum.action.keys()
		for key in keys:
			if event.is_action_pressed(key):
				var first_key : KeyPanel = running_keys.front()
				if first_key.key_name == key && first_key.current_state == 1:
					AudioPlayer.play_audio(first_key.key_name)
					running_keys.erase(first_key)
					first_key.queue_free()
					emit_signal("key_hit", true)
				else:
					running_keys.erase(first_key)
					first_key.queue_free()
					AudioPlayer.play_audio('mute_note')
					emit_signal("key_hit", false)

func _physics_process(delta):
	if running_keys.size() > 0:
		for r in running_keys:
			r.tickets -= delta
			r.rect_position.y += delta * r.speed
			match(r.current_state):
				r.state.RUNNING:
					if (r.tickets + r.HIT_LIMIT) <= 0:
						r.color_key(r.selected_color)
						r.current_state = r.state.IDEAL
				r.state.IDEAL:
					if (r.tickets - r.HIT_LIMIT) <= -2:
						r.color_key(r.error_color)
						r.current_state = r.state.LATE
				r.state.LATE:
					if (r.tickets - r.HIT_LIMIT) <= -3:
						running_keys.erase(r)
						r.queue_free()
	else:
		if keys_queued.size() < 1:
			emit_signal('no_running_keys')

func _process(delta):
	secs_until_next_key -= delta
	if keys_queued.size() > 0 and secs_until_next_key <= 0:
		start_next_key()
		secs_until_next_key = rand_range(BASE_DELAY_MIN, BASE_DELAY_MAX)

func start_next_key():
	var key_panel = keys_queued.pop_front()
	_keys_group.add_child(key_panel)
	key_panel.rect_position.x -= 50
	key_panel.rect_position.y = -150
	key_panel.rect_position.x += key_panel.key * 100
	key_panel.set_name(key_panel.key_name)
	key_panel.connect("key_success", self, '_key_success_received')
	running_keys.push_back(key_panel)

func generate_key():
	var keys =  SpoonKeyEnum.action.keys()
	var key_index = randi() % SpoonKeyEnum.action.size()
	var selected_key = keys[key_index]
	var time = BASE_TIME
	var key_panel : KeyPanel = key_panel_resource.instance()
	key_panel.tickets = BASE_TIME
	key_panel.key = key_index
	key_panel.key_name = selected_key
	keys_queued.append(key_panel)

func generate_chords(number : int):
	for i in range(number):
		generate_key()
